$(function(){

//Fonction pour actualiser le nombre de tâches
function actualiserNbreTaches(){
  let nbreTaches = $('.todo-list li:not(.completed)').length;
  $('.todo-count strong').text(nbreTaches);
}

//AJOUT D'UNE TÂCHE
$('.new-todo').keyup(function(e){
 if (e.keyCode === 13) {
   $.ajax({
      url: 'tasks/add',
      data: {
        content: $(this).val()
      },
      method: 'post',
      context: this
    })
    .done(function(reponsePHP){
      $('.todo-list').prepend(reponsePHP)
                     .find('li:first')
                     .hide()
                     .slideDown(function(){
                       $('.new-todo').val('');
                     });
      actualiserNbreTaches();
    })
    .fail(function(){
      alert('Un problème est survenu durant la transaction.');
    });
 }
});

//SUPPRESSION D'UNE TÂCHE
$('.todo-list').on('click', '.destroy', function(){
  $.ajax({
      url: 'tasks/delete/' + $(this).closest('li').attr('data-id'),
      context: this
   })
   .done(function(reponsePHP){
     $(this).closest('li').slideUp(function(){
                                    $(this).remove();
                                    actualiserNbreTaches();
     });
   })
   .fail(function(){
     alert('Un problème est survenu durant la transaction.');
   });
});

//MODIFICATION D'UNE TÂCHE
//Formulaire de modification
$('.todo-list').on('dblclick', '.todo:not(.todo.completed)', function(){
  let contenu = $(this).find('label').text();
  $(this).find('label').html(`<input type="text" value="${contenu}"/>`);

});
//Validation de la modification
$('.todo-list').on('keyup', '.todo input[type="text"]', function(e){
  if (e.keyCode === 13){
    $.ajax({
        url: 'tasks/edit/' + $(this).closest('li').attr('data-id'),
        data: {
          content: $(this).val()
        },
        method: 'post',
        context: this
     })
     .done(function(reponsePHP){
       let contenu = $(this).val();
       $(this).closest('label').text(contenu);
     })
     .fail(function(){
       alert('Un problème est survenu durant la transaction.');
     });
  }
});

//TERMINER UNE TÂCHE
$('.todo-list').on('click', '.toggle', function(){
  if ($(this).attr('checked') == 'checked') {
    $(this).removeAttr('checked');
    $.ajax({
        url: 'tasks/toggleFinish/' + $(this).closest('li').attr('data-id'),
        data: {
          finished: 0
        },
        method: 'post',
        context: this
     })
     .done(function(reponsePHP){
        $(this).closest('li').removeClass('completed');
        actualiserNbreTaches();
     })
     .fail(function(){
       alert('Un problème est survenu durant la transaction.');
     });
   }
  else {
    $(this).attr('checked', 'checked');
    $.ajax({
        url: 'tasks/toggleFinish/' + $(this).closest('li').attr('data-id'),
        data: {
          finished: 1
        },
        method: 'post',
        context: this
     })
     .done(function(reponsePHP){
        $(this).closest('li').addClass('completed');
        actualiserNbreTaches();
     })
     .fail(function(){
       alert('Un problème est survenu durant la transaction.');
     });
  }
});

//FILTRES
//Toutes les tâches
$('.all').click(function(){
  $('.todo').slideDown();
});
//Tâches en cours
$('.active').click(function(){
  $('.todo.completed').slideUp();
  $('.todo:not(.completed)').slideDown();
});
//Tâches terminées
$('a.completed').click(function(){
  $('.todo:not(.completed)').slideUp();
  $('.todo.completed').slideDown();
});

//FIN
});
